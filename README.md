## INTRODUCTION

@todo: Improve README!

The Billwerk Subscriptions module is a DESCRIBE_THE_MODULE_HERE.

The primary use case for this module is:

- Use case #1
- Use case #2
- Use case #3

## REQUIREMENTS

The module has no other module dependencies.
You should typically have a Billwerk Subscription with a Sandbox and a Live
environment.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

### Drupal Configuration

- Module Settings can be configured at: ´/admin/config/services/billwerk-subscriptions-settings´
  - Configure the settings

- Billwerk Credentials need to be provided in your Drupal settings.php as
follows:
~~~
$settings['billwerk_subscriptions.settings'] = [
  'webhook_secret' => 'RANDOMSTRING', // Use a random URL-safe  (~70 chars).
  'credentials' => [
    'sandbox' => [
      'client_id' => 'YOURSANDBOXCLIENTID',
      'client_secret' => 'YOURSANDBOXCLIENTSECRET',
      'selfservice_public_api_key' => 'YOURSELFSERVICEPUBLICAPIKEY',
    ],
    'production' => [
      'client_id' => 'YOURPRODUCTIONCLIENTID',
      'client_secret' => 'YOURPRODUCTIONCLIENTSECRET',
      'selfservice_public_api_key' => 'YOURSELFSERVICEPUBLICAPIKEY',
    ],
  ],
];

~~~
These credentials can be generated in the Billwerk Settings >
  Billwerk-Apps > My Apps > Add:
- Name: The name, for example your Drupal project name or url.
- Client Type: Confidential
In the next step you can enter your client secret. Keep it safe!


### Billwerk Configuration

To integrate this module with the Billwerk Workflows,
 you should configure the webhooks relevant to your process.
Typically the following Webhooks should be considered:
- ContractCreated
- ContractChanged
- ContractDataChanged
- ContractDeleted
- CustomerCreated
- CustomerChanged
- CustomerDeleted
- RecurringBillingApproaching
- TrialEndApproaching
- CustomerLocked
- CustomerUnlocked

To set up the Webhooks, at Billwerk go to Settings > Integration > Webhooks.
If your project uses one of the authentications, 
select HTTP Basic Auth (e.g. with Shield module in development) or Azure Auth.
Otherwise select "No authentication" and enter your webhook URL:
`https://www.example.com/billwerk-subscriptions/webhook-listener/{secret}`
Replace `{secret}` with the `webhook_secret` from your settings (see above)!

## CONCEPTS

### Billwerk as primary / leading system
Billwerk is treated as primary system for handling and providing customer data.
 Drupal consumes data from Billwerk. For that reason:
  - User details, especially for billing, should be stored in Billwerk.
   If required in Drupal, it should be fetched from there
  - Changing user data should typically be done at Billwerk

### Representative Billwerk Contract links the User with Billwerk
The logical connection between Billwerk and Drupal happens
 through the *representative Contract* at Billwerk that is referenced by
  its Contract ID stored in the Drupal user profile field: 
  *field_billwerk_contract_id*.
  As Billwerk allows for:
  - Multiple Customers with the same email address
  - Multiple Contracts for the same customer
  this would otherwise introduce multiple possible edge-cases 
  (even contradictions),
   if the connection was at another level (e.g. User <--> Customer).

#### Setting / retrieving the representative Billwerk Contract ID for a Drupal user

The Billwerk Contract ID (*field_billwerk_contract_id*) 
can be set manually or automatically in the Drupal user profile.
The contract ID is automatically set in the following cases:
- If the Billwerk Webhook "ContractCreated" is called:
  - A new Drupal user is created with the email address 
  and details from Billwerk.
   On account creation the Billwerk Contract ID is stored in the user profile
  - An existing Drupal user is updated with the details from Billwerk,
   if it has the same email address like in Billwerk 
    AND *field_billwerk_contract_id* is empty.
- If the *Lookup Billwerk Contract* action is executed on the Drupal user.
  If the *field_billwerk_contract_id* is already filled, 
  it will only be overwritten, if the overwrite option is enabled.

Alternatively, the field_billwerk_contract_id field can be set manually.

### Default Billwerk webhook handling

The module will by default handle the following Billwerk
 Webhooks in the described way:

Please remember to set up the webhooks you wish to use at
 Billwerk as described in the CONFIGURATION > Billwerk chapter.

### Assumptions and limitations

- Billwerk does not prevent multiple Customers with the same
 EmailAddress or ExternalCustomerId! For that reasons you have 
 to ensure this never happens!
- Billwerk allows Customers to have more than one Contract!
 For that reason we're referencing the representative ContractId 
   in Drupal user accounts and not 
- Editing Customers, Contracts, etc. in Billwerk should always 
be done with care. Know what you're doing, as if you break assumptions,
 it may have unwanted /unexpected side-effects!
- Due to the listed assumptions and limitations there may be cases where 
one Drupal account could potentially match multiple Customers or 
Contracts in Billwerk (especially, if you're making manual additions or 
changes in Billwerk). For that reason:
  - Always ensure that the Billwerk Customer's ExternalCustomerId is 
  always unique and matches the Drupal user id of the associated user
  - Always ensure that the Drupal *field_billwerk_contract_id*
   matches the correct contract.
  **These are the two primary factors used to connect the Drupal user** 
  **with the Billwerk Customer!**
  

### TIPS & TRICKS

### Debugging API Calls
- 

## MAINTAINERS

Current maintainers for Drupal 10:

- FIRST_NAME LAST_NAME (NICKNAME) - https://www.drupal.org/u/NICKNAME
