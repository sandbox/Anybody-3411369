<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure billwerk_subscriptions_manage settings.
 */
final class SettingsForm extends ConfigFormBase {

  const CANCELLATION_METHOD_DOWNGRADE = 'downgrade';
  const CANCELLATION_METHOD_CANCEL_CONTRACT = 'cancel_contract';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_manage_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['billwerk_subscriptions_manage.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['settings']['cancellation_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('When canceling a subscription plan:'),
      '#options' => [
        self::CANCELLATION_METHOD_DOWNGRADE => $this->t('Downgrade to a free basic plan variant'),
        self::CANCELLATION_METHOD_CANCEL_CONTRACT => $this->t('Cancel the contract entirely (not yet implemented)'),
      ],
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('cancellation_method'),
      '#required' => TRUE,
      // This option is not yet implemented:
      'cancel_contract' => ['#disabled' => TRUE],
    ];

    $form['settings']['billwerk_free_plan_variant_id_sandbox'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Billwerk Plan Variant ID of your basic free plan for downgrade (Sandbox)'),
      '#description' => $this->t('The Billwerk Plan Variant ID of your free basic plan. Enter the plan variant id from the Sandbox environment here. <em>Leave empty to NOT create Billwerk Customer / Contract on Drupal user registration.</em>'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('billwerk_free_plan_variant_id_sandbox'),
      '#states' => [
        'visible' => [
          ':input[name="cancellation_method"]' => ['value' => SettingsForm::CANCELLATION_METHOD_DOWNGRADE],
        ],
        'required' => [
          ':input[name="cancellation_method"]' => ['value' => SettingsForm::CANCELLATION_METHOD_DOWNGRADE],
        ],
      ],
    ];
    $form['settings']['billwerk_free_plan_variant_id_production'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Billwerk Plan Variant ID of your basic free plan for downgrade (Production)'),
      '#description' => $this->t('The Billwerk Plan Variant ID of your free basic plan. Enter the plan variant id from the Production environment here. <em>Leave empty to NOT create Billwerk Customer / Contract on Drupal user registration.</em>'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('billwerk_free_plan_variant_id_production'),
      '#states' => [
        'visible' => [
          ':input[name="cancellation_method"]' => ['value' => SettingsForm::CANCELLATION_METHOD_DOWNGRADE],
        ],
        'required' => [
          ':input[name="cancellation_method"]' => ['value' => SettingsForm::CANCELLATION_METHOD_DOWNGRADE],
        ],
      ],
    ];

    $form['order_terms'] = [
      '#type' => 'text_format',
      '#title' => $this->t('General order terms'),
      '#description' => $this->t('Order terms to show on the change subscription form.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms.format'),
    ];

    $form['order_terms_upgrade'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Upgrade order terms'),
      '#description' => $this->t('Order terms for upgrades to show on the change subscription form.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms_upgrade.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms_upgrade.format'),
    ];

    $form['order_terms_downgrade'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Downgrade order terms'),
      '#description' => $this->t('Order terms for downgrades to show on the change subscription form.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms_downgrade.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('order_terms_downgrade.format'),
    ];

    $form['subscription_cancel_downgrade_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Subscription cancellation downgrade message'),
      '#description' => $this->t('The message shown before requesting a downgrade cancellation.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_cancel_downgrade_message.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_cancel_downgrade_message.format'),
    ];

    $form['subscription_cancel_contract_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Subscription contract cancellation message'),
      '#description' => $this->t('The message shown before requesting a contract cancellation.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_cancel_contract_message.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_cancel_contract_message.format'),
    ];

    $form['subscription_changed_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Subscription changed confirmation message'),
      '#description' => $this->t('The message shown after a change to the subscription has been made.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_changed_message.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_changed_message.format'),
    ];

    $form['subscription_canceled_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Cancellation successful message'),
      '#description' => $this->t('The message shown after a cancellation has been made successfully using the "Cancel subscription" form.'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_canceled_message.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('subscription_canceled_message.format'),
    ];

    // The following settings are the same as in the block:
    $form['page']['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('"Manage my subscription" page settings'),
      '#open' => TRUE,
      '#description' => $this->t('Settings for the "Manage my subscription"-page (which is also available as block)'),
      '#description_display' => 'after',
    ];
    $form['page']['settings']['manage_subscriptions_link'] = [
      '#type' => 'select',
      '#title' => $this->t('Show on "Manage subscriptions" page'),
      '#options' => [
        '' => $this->t('Do not show'),
        'link' => $this->t('Show as button / link'),
        'form' => $this->t('Show embedded'),
      ],
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.manage_subscriptions_link'),
    ];
    $form['page']['settings']['source'] = [
      '#type' => 'select',
      '#title' => $this->t('User source'),
      '#options' => [
        'current' => $this->t('Current user'),
        'param_user' => $this->t('Route user account (Route parameter "user)'),
      ],
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.source'),
    ];
    $form['page']['settings']['show_active_subscription'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show active subscriptions'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_active_subscription'),
    ];
    $form['page']['settings']['show_active_components'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show active components'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_active_components'),
    ];
    $form['page']['settings']['no_active_components_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('No active components text'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.no_active_components_text.value'),
      '#format' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.no_active_components_text.format'),
      '#description' => $this->t('If left empty, the components list will be hidden.'),
    ];
    $form['page']['settings']['show_change_plan_variant_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "Change plan" button'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_change_plan_variant_button'),
    ];
    $form['page']['settings']['show_cancel_plan_variant_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "Cancel subscription" button'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_cancel_plan_variant_button'),
    ];
    $form['page']['settings']['show_components_subscribe_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "Subscribe add-ons" button'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_components_subscribe_button'),
    ];
    $form['page']['settings']['show_components_unsubscribe_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "Cancel add-on" button'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.show_components_unsubscribe_button'),
    ];
    $form['page']['settings']['modal_actions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open the actions in a modal'),
      '#default_value' => $this->config('billwerk_subscriptions_manage.settings')->get('page.settings.modal_actions'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('billwerk_subscriptions_manage.settings')
      ->set('cancellation_method', $form_state->getValue('cancellation_method'))
      ->set('billwerk_free_plan_variant_id_sandbox', $form_state->getValue('billwerk_free_plan_variant_id_sandbox'))
      ->set('billwerk_free_plan_variant_id_production', $form_state->getValue('billwerk_free_plan_variant_id_production'))
      ->set('order_terms', $form_state->getValue('order_terms'))
      ->set('order_terms_upgrade', $form_state->getValue('order_terms_upgrade'))
      ->set('order_terms_downgrade', $form_state->getValue('order_terms_downgrade'))
      ->set('subscription_changed_message', $form_state->getValue('subscription_changed_message'))
      ->set('confirmation_message_downgrade', $form_state->getValue('confirmation_message_downgrade'))
      ->set('subscription_canceled_message', $form_state->getValue('subscription_canceled_message'))
      ->set('page.settings.manage_subscriptions_link', $form_state->getValue('manage_subscriptions_link'))
      ->set('page.settings.source', $form_state->getValue('source'))
      ->set('page.settings.show_active_subscription', $form_state->getValue('show_active_subscription'))
      ->set('page.settings.show_active_components', $form_state->getValue('show_active_components'))
      ->set('page.settings.no_active_components_text', $form_state->getValue('no_active_components_text'))
      ->set('page.settings.show_change_plan_variant_button', $form_state->getValue('show_change_plan_variant_button'))
      ->set('page.settings.show_cancel_plan_variant_button', $form_state->getValue('show_cancel_plan_variant_button'))
      ->set('page.settings.show_components_subscribe_button', $form_state->getValue('show_components_subscribe_button'))
      ->set('page.settings.show_components_unsubscribe_button', $form_state->getValue('show_components_unsubscribe_button'))
      ->set('page.settings.modal_actions', $form_state->getValue('modal_actions'))

      ->save();
    parent::submitForm($form, $form_state);
  }

}
