<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a Billwerk Subscriptions Manage form.
 */
final class ComponentsSubscribeForm extends ChangeFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_manage_components_subscribe';
  }

  /**
   * {@inheritdoc}
   */
  public function buildSelectionForm(array $form, FormStateInterface $form_state): array {
    $this->buildComponentsSubscribeFormElement($form, $form_state);
    if (empty($form['components_subscribe']['#options'])) {
      return [
        'message' => [
          '#type' => 'inline_template',
          '#template' => '<div class="messages messages--warning"><div class="message">{{ "No (further) add-ons available for your subscription.\n\nCanceled add-ons can only be re-booked after expiration. Please contact us to revoke cancellations or for individual cases."|t|nl2br }}</div></div>',
          '#context' => [],
        ],
      ];
    }
    $form['components_subscribe']['#required'] = TRUE;
    $this->buildCouponFormElement($form, $form_state);

    $form['actions']['#type'] = 'actions';
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
          'button--cancel',
        ],
      ],
      '#url' => Url::fromRoute('billwerk_subscriptions_manage.current_user_subscription'),
      // Adjust the weight to change the position of the button.
      '#weight' => -1,
    ];
    $form['actions']['proceed'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#validate' => ['::validateSelectionForm'],
      '#submit' => ['::proceedToConfirm'],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Validate the selection form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSelectionForm(array &$form, FormStateInterface $form_state) {
    $activeComponents = $this->subscriberBillwerkEntitiesHelper->getActiveBillwerkComponents();
    $selectedComponents = array_filter($form_state->getValue('components_subscribe'));
    if (!empty(array_diff_key($selectedComponents, $this->billwerkEntitiesHelper->getComponentsSubscribeSelectOptions(array_keys($activeComponents))))) {
      $form_state->setErrorByName('components_subscribe', $this->t('Selected invalid component.'));
    }
  }

  /**
   * Build the confirm form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildConfirmForm(array $form, FormStateInterface $form_state): array {
    $newComponentSubscriptionIds = $this->valueGetComponentsSubscribe($form_state);
    $couponcode = $this->valueGetCouponcode($form_state);

    $billwerkOrderObj = $this->subscriber->billwerkChangeSubscription(
      NULL,
      $newComponentSubscriptionIds,
      [],
      // Coupon:
      $couponcode,
      // Subscribe to addon immediately (paid):
      TRUE,
      // We need a committable (non-preview) order:
      FALSE,
      // We commit this order by JavaScript (after payment):
      FALSE,
    );

    $this->buildOrderTableFormElement($form, $form_state, $billwerkOrderObj);
    $this->buildOrderAddressFormElement($form, $form_state, $billwerkOrderObj);
    $this->buildOrderPaymentFormElement($form, $form_state, $billwerkOrderObj);
    $this->buildUpgradeOrderTermsFormElement($form, $form_state);
    // Here we use the fake submit, as payment and submit need to be
    // client-side:
    $this->buildOrderActionsFakeFormElement($form, $form_state, $this->t('Buy'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // @todo Presumably this is never called?
    // $this->messenger()->addStatus($this->t('The message has been sent.'));
    throw new \Exception('This was not expected to be called!');
    // $form_state->setRedirect('<front>');
  }

}
