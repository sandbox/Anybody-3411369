<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a Billwerk Subscriptions Manage form.
 */
class ChangePlanVariantForm extends ChangeFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_manage_change_plan_variant';
  }

  /**
   * {@inheritdoc}
   */
  public function buildSelectionForm(array $form, FormStateInterface $form_state): array {
    if ($this->subscriber->billwerkHasPendingContractPhases()) {
      return [
        'message' => [
          '#type' => 'inline_template',
          '#template' => '<div class="messages messages--warning"><div class="message">{{ "You have pending contract changes. Further contract amendments are not possible until then. Please contact us to revoke cancellations or for individual cases."|t }}</div></div>',
          '#context' => [],
        ],
      ];
    }

    $this->buildPlanVariantChangeFormElement($form, $form_state);
    $this->buildComponentsSubscribeFormElement($form, $form_state);
    $this->buildCouponFormElement($form, $form_state);

    $form['actions']['#type'] = 'actions';
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
          'button--cancel',
        ],
      ],
      '#url' => Url::fromRoute('billwerk_subscriptions_manage.current_user_subscription'),
      // Adjust the weight to change the position of the button.
      '#weight' => -1,
    ];
    $form['actions']['proceed'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#validate' => ['::validateSelectionForm'],
      '#submit' => ['::proceedToConfirm'],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Validate the selection form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSelectionForm(array &$form, FormStateInterface $form_state) {
    $activePlanVariant = $this->subscriberBillwerkEntitiesHelper->getActiveBillwerkPlanVariant();
    $selectedPlanVariant = $form_state->getValue('planvariant');

    if ($activePlanVariant->getMachineName() === $selectedPlanVariant) {
      $form_state->setErrorByName('planvariant', $this->t('You did not select a different plan variant.'));
    }

    if (!in_array($selectedPlanVariant, array_keys($this->billwerkEntitiesHelper->getPlanVariantsAllSelectOptions()))) {
      $form_state->setErrorByName('planvariant', $this->t('Selected invalid plan variant.'));
    }

    $activeComponents = $this->subscriberBillwerkEntitiesHelper->getActiveBillwerkComponents();
    $selectedComponents = array_filter($form_state->getValue('components_subscribe') ?? []);
    if (!empty($selectedComponents) && !empty(array_diff_key($selectedComponents, $this->billwerkEntitiesHelper->getComponentsSubscribeSelectOptions(array_keys($activeComponents))))) {
      $form_state->setErrorByName('components_subscribe', $this->t('Selected invalid component to book.'));
    }
  }

  /**
   * Build the confirm form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildConfirmForm(array $form, FormStateInterface $form_state): array {
    $newPlanVariantId = $this->valueGetPlanvariant($form_state);
    $newComponentSubscriptionIds = $this->valueGetComponentsSubscribe($form_state);
    $endComponentSubscriptionIds = $this->valueGetComponentsUnsubscribe($form_state);
    $couponcode = $this->valueGetCouponcode($form_state);

    $activePlanVariant = $this->subscriberBillwerkEntitiesHelper->getActiveBillwerkPlanVariant();
    $selectedPlanVariant = $form_state->getValue('planvariant');

    $isUpgrade = $activePlanVariant->isUpgrade($this->billwerkEntitiesHelper->getBillwerkPlanVariantByMachineName($selectedPlanVariant));

    $billwerkOrderObj = $this->subscriber->billwerkChangeSubscription(
    $newPlanVariantId,
    $newComponentSubscriptionIds,
    $endComponentSubscriptionIds,
    $couponcode,
      // Upgrade immediately, but downgrade at the end of the contract period!
      $isUpgrade,
      // We need a committable (non-preview) order:
      FALSE,
      // We commit this order by JavaScript (after payment):
      FALSE,
    );

    // @codingStandardsIgnoreStart
    // @todo
    // dsm($isUpgrade);
    // dsm([
    //   '$newPlanVariantId' => $newPlanVariantId,
    //   '$newComponentSubscriptionIds' => $newComponentSubscriptionIds,
    //   '$endComponentSubscriptionIds' => $endComponentSubscriptionIds,
    //   '$selectedCouponCode' => $couponcode,
    // ]);
    // dsm($billwerkOrderObj);
    // @codingStandardsIgnoreEnd

    // @todo If the result is a 0 € bill, show a description instead of the
    // table!
    $this->buildOrderTableFormElement($form, $form_state, $billwerkOrderObj);
    $this->buildOrderAddressFormElement($form, $form_state, $billwerkOrderObj);
    $this->buildOrderPaymentFormElement($form, $form_state, $billwerkOrderObj);
    if ($isUpgrade) {
      // Upgrade order terms text:
      $this->buildUpgradeOrderTermsFormElement($form, $form_state);
    }
    else {
      // Downgrade order terms text:
      $this->buildDowngradeOrderTermsFormElement($form, $form_state);
    }
    // Here we use the fake submit, as payment and submit need to be
    // client-side:
    $this->buildOrderActionsFakeFormElement($form, $form_state, $this->t('Buy'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // @todo Presumably this is never called?
    // $this->messenger()->addStatus($this->t('The message has been sent.'));
    throw new \Exception('This was not expected to be called!');
    // $form_state->setRedirect('<front>');
  }

}
