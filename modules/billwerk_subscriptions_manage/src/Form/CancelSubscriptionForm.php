<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\billwerk_subscriptions\Exception\EnvironmentException;
use Drupal\billwerk_subscriptions\Exception\SubscriberException;

/**
 * Provides a Billwerk Subscriptions Manage form.
 */
final class CancelSubscriptionForm extends ChangePlanVariantForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'billwerk_subscriptions_manage_cancel_subscription';
  }

  /**
   * {@inheritdoc}
   */
  public function buildSelectionForm(array $form, FormStateInterface $form_state): array {
    $this->buildCancellationMessage($form, $form_state);

    $form['actions']['#type'] = 'actions';
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
          'button--cancel',
        ],
      ],
      '#url' => Url::fromRoute('billwerk_subscriptions_manage.current_user_subscription'),
      // Adjust the weight to change the position of the button.
      '#weight' => -1,
    ];

    $form['actions']['proceed'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#validate' => ['::validateSelectionForm'],
      '#submit' => ['::proceedToConfirm'],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Validate the selection form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSelectionForm(array &$form, FormStateInterface $form_state) {
    $activePlanVariant = $this->subscriberBillwerkEntitiesHelper->getActiveBillwerkPlanVariant();
    $selectedPlanVariant = $form_state->getValue('planvariant');

    if ($activePlanVariant === $selectedPlanVariant) {
      $form_state->setErrorByName('planvariant', $this->t('You did not change the plan variant.'));
    }
  }

  /**
   * Build the confirm form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildConfirmForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory()->get('billwerk_subscriptions_manage.settings');
    // @improve: Inject the Helper service instead and use its helper method here:
    $cancellationMethod = $config->get('cancellation_method');
    if ($cancellationMethod === SettingsForm::CANCELLATION_METHOD_DOWNGRADE) {
      // @improve: Inject the Helper service instead and use its helper method here:
      if ($this->environment->isProduction()) {
        $newPlanVariantId = $config->get('billwerk_free_plan_variant_id_production');
      }
      elseif ($this->environment->isSandbox()) {
        $newPlanVariantId = $config->get('billwerk_free_plan_variant_id_sandbox');
      }
      else {
        throw new EnvironmentException('Unknown environment: ' . $this->environment->getEnvironmentName());
      }
      if (empty($newPlanVariantId)) {
        throw new SubscriberException('Registration plan variant for environment: ' . $this->environment->getEnvironmentName() . ' is empty. Order creation was not possible. Check your settings.');
      }

      $endComponentSubscriptionIds = [];
      $activeBillwerkComponentSubscriptions = $this->subscriber->getBillwerkContract()->getBillwerkContractSubscription()->getComponentSubscriptions();
      foreach ($activeBillwerkComponentSubscriptions as $activeBillwerkComponentSubscriptions) {
        $endComponentSubscriptionIds[] = $activeBillwerkComponentSubscriptions->getId();
      }

      // Downgrade to basic free subscription:
      $billwerkOrderObj = $this->subscriber->billwerkChangeSubscription(
        $newPlanVariantId,
        [],
        $endComponentSubscriptionIds,
        // No coupon:
        '',
        // Cancellation should happen at the end of the contract period:
        FALSE,
        // We need a committable (non-preview) order:
        FALSE,
        // We commit this order by JavaScript:
        FALSE,
      );
      $form_state->set('billwerkOrder', $billwerkOrderObj);
      $form_state->set('step', 'confirm');

      $this->buildOrderTableFormElement($form, $form_state, $billwerkOrderObj);
      if (!$billwerkOrderObj->AllowWithoutPaymentData) {
        $this->buildOrderPaymentFormElement($form, $form_state, $billwerkOrderObj);
        $this->buildOrderAddressFormElement($form, $form_state, $billwerkOrderObj);
      }
      $this->buildDowngradeOrderTermsFormElement($form, $form_state);
    }
    elseif ($cancellationMethod === SettingsForm::CANCELLATION_METHOD_CANCEL_CONTRACT) {
      // @todo Someone should implement this, if needed. We don't use this.
    }

    if (!$billwerkOrderObj->AllowWithoutPaymentData) {
      // There are edge-cases where even for a downgrade
      // payment data is requested:
      // Here we use the fake submit, as payment and submit need to be
      // client-side:
      $this->buildOrderActionsFakeFormElement($form, $form_state, $this->t('Confirm subscription cancellation'));
    }
    else {
      $this->buildOrderActionsFormElement($form, $form_state, $this->t('Confirm subscription cancellation'));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $billwerkOrderObj = $form_state->get('billwerkOrder');
    if (!empty($billwerkOrderObj) && !empty($billwerkOrderObj->Id)) {
      // We can commit the order server-side here, because no payment data is
      // needed!
      $this->api->commitOrder($billwerkOrderObj->Id, []);
      $confirmationMessage = $this->config('billwerk_subscriptions_manage.settings')->get('subscription_canceled_message');
      $this->messenger()->addStatus($confirmationMessage['value'] ?: $this->t('Your subscription has been canceled to the end of the contract.'));
      $form_state->setRedirect('billwerk_subscriptions_manage.current_user_subscription');
    }
    else {
      throw new SubscriberException('Order was not committed, because order id could not be determined.');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getProviderReturnUrl(): string {
    // @improve: Check if this is ever called here!
    return Url::fromRoute('billwerk_subscriptions_manage.current_user_subscription_finished_cancel')->setAbsolute(TRUE)->toString();
  }

  /**
   * Build the cancellation message.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildCancellationMessage(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->get('billwerk_subscriptions_manage.settings');
    $cancellationMethod = $config->get('cancellation_method');
    if ($cancellationMethod === SettingsForm::CANCELLATION_METHOD_DOWNGRADE) {
      $form['message'] = [
        '#type' => 'processed_text',
        '#text' => $config->get('subscription_cancel_downgrade_message.value'),
        '#format' => $config->get('subscription_cancel_downgrade_message.format'),
        '#prefix' => '<div class="messages messages--status"><div class="message">',
        '#suffix' => '</div></div>',
      ];
    }
    elseif ($cancellationMethod === SettingsForm::CANCELLATION_METHOD_CANCEL_CONTRACT) {
      $form['message'] = [
        '#type' => 'processed_text',
        '#text' => $config->get('subscription_cancel_contract_message.value'),
        '#format' => $config->get('subscription_cancel_contract_message.format'),
        '#prefix' => '<div class="messages messages--status"><div class="message">',
        '#suffix' => '</div></div>',
      ];
    }
    else {
      throw new \UnexpectedValueException("Unexpected cancellation method: \"{$cancellationMethod}\"");
    }
  }

}
