<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_manage;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\billwerk_subscriptions\Exception\EnvironmentException;

/**
 * Provides utility functions for the Billwerk Subscriptions module.
 *
 * This class contains helper methods that facilitate integration and data
 * handling between Drupal and Billwerk.
 *
 * @todo Add class description.
 */
final class Helper {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a Helper object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Environment $environment,
  ) {
    $this->config = $configFactory->get('billwerk_subscriptions_manage.settings');
  }

  /**
   * Returns the configured free plan variant from the settings.
   *
   * @return string
   *   The free plan variant ID for the current environment.
   */
  public function getBillwerkFreePlanVariantId(): string {
    if ($this->environment->isProduction()) {
      return $this->config->get('billwerk_free_plan_variant_id_production');
    }
    elseif ($this->environment->isSandbox()) {
      return $this->config->get('billwerk_free_plan_variant_id_sandbox');
    }
    else {
      throw new EnvironmentException('Unknown environment: ' . $this->environment->getEnvironmentName());
    }
  }

  /**
   * Returns the configured subscription cancellation method from the settings.
   *
   * @return string
   *   The configured subscription cancellation method.
   */
  public function getCancellationMethod(): string {
    return $this->config->get('cancellation_method');
  }

}
