<?php

namespace Drupal\Tests\billwerk_subscriptions_manage\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for billwerk_subscriptions_manage.
 *
 * @group billwerk_subscriptions_manage
 */
class BillwerkSubscriptionsManageGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
