<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_handler_default\EventSubscriber;

use Drupal\billwerk_subscriptions\Event\SubscriberCustomerChangedEvent;
use Drupal\billwerk_subscriptions\LogHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to customer changes.
 *
 * Updates the user, whenever the Billwerk Customer changed.
 */
class SubscriberCustomerChangedEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a SubscriberCustomerChangedEventSubscriber object.
   */
  public function __construct(
    protected readonly LogHelper $logHelper,
  ) {}

  /**
   * Handles the subscriber customer changed event.
   *
   * @param \Drupal\billwerk_subscriptions\Event\SubscriberCustomerChangedEvent $event
   *   The subscriber customer changed event.
   */
  public function onSubscriberCustomerChangedEvent(SubscriberCustomerChangedEvent $event): void {
    // @improve: We should sort out changes that are not relevant to the subscription, if it is possible and makes sense.
    // Trigger a refresh of the subscriber:
    $event->getSubscriber()->refreshFromBillwerkContractSubscription();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SubscriberCustomerChangedEvent::class => ['onSubscriberCustomerChangedEvent'],
    ];
  }

}
