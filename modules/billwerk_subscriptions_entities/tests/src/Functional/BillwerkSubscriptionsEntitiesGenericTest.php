<?php

namespace Drupal\Tests\billwerk_subscriptions_entities\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for billwerk_subscriptions_entities.
 *
 * @group billwerk_subscriptions_entities
 */
class BillwerkSubscriptionsEntitiesGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
