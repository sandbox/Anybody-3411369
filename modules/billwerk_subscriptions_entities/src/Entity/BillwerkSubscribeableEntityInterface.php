<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Entity;

use Drupal\Core\Session\AccountProxyInterface;

/**
 * The BillwerkSubscribeableEntity interface.
 */
interface BillwerkSubscribeableEntityInterface extends BillwerkEntityInterface {

  /**
   * Determines if the user is subscribed to this.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to check.
   *
   * @return bool
   *   Is subscribed?
   */
  public function isSubscribed(AccountProxyInterface $user): bool;

  /**
   * Determines if the user can subscribe to this.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to perform the operation.
   *
   * @return bool
   *   Can unsubscribe?
   */
  public function accessSubscribe(AccountProxyInterface $user): bool;

  /**
   * Determines if the user can unsubscribe from this.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to perform the operation.
   *
   * @return bool
   *   Can unsubscribe?
   */
  public function accessUnsubscribe(AccountProxyInterface $user): bool;

  /**
   * Helper function to build the upgrade button as Drupal Render array.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The user to perform the operation.
   * @param bool $modal
   *   Show as modal?
   * @param array $additionalClasses
   *   Additional classes to add.
   *
   * @return array
   *   The Drupal render array.
   */
  public function buildSubscribeButton(
    AccountProxyInterface $user,
    bool $modal = FALSE,
    array $additionalClasses = ['secondary', 'expanded'],
  ): array;

}
