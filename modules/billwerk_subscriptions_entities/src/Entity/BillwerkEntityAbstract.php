<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\billwerk_subscriptions\Api;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\user\EntityOwnerTrait;

/**
 * The Billwerk entity abstract class.
 */
abstract class BillwerkEntityAbstract extends RevisionableContentEntityBase implements BillwerkEntityInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * The BillwerkAPI object.
   *
   * @var \Drupal\billwerk_subscriptions\Api
   */
  protected Api $billwerkApi;


  /**
   * The environment.
   *
   * @var \Drupal\billwerk_subscriptions\Environment
   */
  protected Environment $environment;

  /**
   * {@inheritDoc}
   */
  public function __construct($values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    $this->billwerkApi = \Drupal::service('billwerk_subscriptions.api');
    $this->environment = \Drupal::service('billwerk_subscriptions.environment');
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function labelTranslated(?string $langcode = NULL): string {
    if ($langcode === NULL) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }
    return $this->hasTranslation($langcode) ? $this->getTranslation($langcode)->label() : $this->label();
  }

  /**
   * {@inheritDoc}
   */
  public function getMachineName(): string {
    return $this->get('machine_name')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function isEnabled(): bool {
    return !empty($this->get('status')->value);
  }

  /**
   * {@inheritDoc}
   */
  public function isHidden(): bool {
    return !empty($this->get('hidden')->value);
  }

  /**
   * {@inheritDoc}
   */
  public function getWeight(): int {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getNotesInternal(): string {
    return $this->get('field_notes_internal')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getSubtitle(): string {
    return $this->get('field_subtitle')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkId(?string $environmentName = NULL): string {
    if ($environmentName === NULL) {
      $environmentName = $this->environment->getEnvironmentName();
    }
    if ($environmentName === Environment::ENVIRONMENT_PRODUCTION) {
      return $this->get('billwerk_id_production')->value;
    }
    elseif ($environmentName === Environment::ENVIRONMENT_SANDBOX) {
      return $this->get('billwerk_id_sandbox')->value;
    }
    else {
      throw new \Exception('Unknown environment name: ' . $environmentName);
    }
  }

  /**
   * Fetches the Billwerk raw saas data as array for this type.
   *
   * @param string $environmentName
   *   The environment: sandbox|production.
   *
   * @return array
   *   The Billwerk data array or an empty array, if not found.
   */
  protected function fetchBillwerkProductInfo(?string $environmentName = NULL): array {
    if ($environmentName === NULL) {
      $environmentName = $this->environment->getEnvironmentName();
    }

    $billwerkProductInfoType = $this->getBillwerkProductInfoType();
    if (!in_array($billwerkProductInfoType, ['PlanGroups', 'Plans', 'PlanVariants', 'Components', 'TaxPolicies'])) {
      throw new \Exception('Unknown product info type: ' . $billwerkProductInfoType);
    }
    $productInfo = $this->billwerkApi->getProductInfo();
    $self = $this;
    $data = array_filter($productInfo[$billwerkProductInfoType], function ($array) use ($self, $environmentName) {
      return ($array['Id'] === $self->getBillwerkId($environmentName));
    });

    return is_array($data) ? array_pop($data) : [];
  }

  /**
   * Gets (and caches) the Billwerk product info.
   *
   * @param string $environmentName
   *   The environment: sandbox|production.
   * @param int $cacheFor
   *   The amount of seconds to cache the result for. Use 0 (carefully!) to
   *   disable cache and return a fresh result. Default is 1 day.
   *
   * @return array
   *   The (eventually cached) Billwerk data array.
   */
  public function getBillwerkProductInfo(?string $environmentName = NULL, int $cacheFor = 86400): array {
    if ($environmentName === NULL) {
      $environmentName = $this->environment->getEnvironmentName();
    }

    $cacheId = 'billwerk_subscriptions_entities:' . $this->getBillwerkId($environmentName) . ':data';
    if ($cacheFor > 0 && $cache = \Drupal::cache()->get($cacheId)) {
      return $cache->data;
    }
    else {
      $cacheTags = [$cacheId];
      $data = $this->fetchBillwerkProductInfo($environmentName);
      $oneDay = time() + $cacheFor;
      \Drupal::cache()->set($cacheId, $data, $oneDay, $cacheTags);
      return $data;
    }
  }

  /**
   * Returns this types key for Billwerk ProductInfo call.
   *
   * This is one of: 'PlanGroups', 'Plans', 'PlanVariants', 'Components',
   * 'TaxPolicies'.
   * If you leave this empty, you can not use $this->fetchProductInfo().
   *
   * @return string
   *   The Billwerk product info type.
   */
  abstract protected function getBillwerkProductInfoType(): string;

}
