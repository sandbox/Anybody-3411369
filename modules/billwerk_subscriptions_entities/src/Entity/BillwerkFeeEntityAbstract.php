<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Entity;

use Drupal\user\Entity\Role;

/**
 * The BillwerkFeeEntityInterface.
 */
abstract class BillwerkFeeEntityAbstract extends BillwerkEntityAbstract implements BillwerkFeeEntityInterface {

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoRecurringPricePerMonthCents(?string $environment = NULL): int {
    $price = $this->getBillwerkProductInfoRecurringPriceCents($environment);
    if ($price === 0) {
      // If the price is zero (free), it's always free!
      return 0;
    }
    $priceUnit = $this->getBillwerkProductInfoRecurringPeriodUnit($environment);
    $priceUnitQuantity = $this->getBillwerkProductInfoRecurringPeriodQuantity($environment);

    $pricePerMonth = NULL;
    switch ($priceUnit) {
      case 'Day':
        throw new \Exception('Can not calculate the price per month based on days.');

      case 'Week':
        throw new \Exception('Can not calculate the price per month based on weeks.');

      case 'Month':
        $pricePerMonth = $price / $priceUnitQuantity;
        break;

      case 'Year':
        $pricePerMonth = $price / $priceUnitQuantity / 12;
        break;
    }
    return $pricePerMonth;
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoRecurringPeriodUnit(?string $environmentName = NULL): string {
    $billwerkData = $this->getBillwerkProductInfo($environmentName);
    // @improve: It's unclear, if this is the right variable, there are some others with similar names:
    return $billwerkData['FeePeriod']['Unit'];
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoRecurringPeriodQuantity(?string $environmentName = NULL): int {
    $billwerkData = $this->getBillwerkProductInfo($environmentName);
    // @improve: It's unclear, if this is the right variable, there are some others with similar names:
    return $billwerkData['FeePeriod']['Quantity'];
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoPaymentPeriodMode(?string $environmentName = NULL): string {
    $billwerkData = $this->getBillwerkProductInfo($environmentName);
    return $billwerkData['PaymentPeriodMode'] ?? NULL;
  }

  /**
   * Returns the role to assign to subscribers of this plan variant.
   *
   * Role is a required field.
   *
   * @return \Drupal\user\Entity\Role
   *   The role.
   */
  public function getRole(): Role {
    $roleId = $this->getRoleId();
    /** @var \Drupal\user\Entity\Role $role */
    $role = $this->entityTypeManager()->getStorage('user_role')->load($roleId);
    return $role;
  }

  /**
   * Returns the role id (string )to assign to subscribers of this component.
   *
   * Role is a required field.
   *
   * @return string
   *   The role id.
   */
  public function getRoleId(): string {
    return $this->get('role')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoCurrencyCodeIso(?string $environment = NULL): string {
    return 'EUR';
  }

  /**
   * {@inheritDoc}
   */
  public function getBillwerkProductInfoCurrencySymbol(?string $environment = NULL): string {
    return '€';
  }

}
