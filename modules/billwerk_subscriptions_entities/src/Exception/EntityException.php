<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities\Exception;

/**
 * Billwerk Subscription Entity Exception.
 */
class EntityException extends \Exception {

}
