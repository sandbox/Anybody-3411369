<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions_entities;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\billwerk_subscriptions\Environment;
use Drupal\billwerk_subscriptions\Subscriber;
use Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan;
use Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant;

/**
 * Helper to interact with the subscriber-related Billwerk Entities.
 */
class SubscriberBillwerkEntitiesHelper {
  use DependencySerializationTrait;

  /**
   * Constructs a SubscriptionPlansManager object.
   */
  public function __construct(
    protected readonly Subscriber $subscriber,
    protected readonly BillwerkEntitiesHelper $billwerkEntitiesHelper,
    protected readonly Environment $environment,
  ) {
  }

  /**
   * Creates a new SubscriptionPlansManager object.
   *
   * @param \Drupal\billwerk_subscriptions\Subscriber $subscriber
   *   The subscriber.
   * @param \Drupal\billwerk_subscriptions_entities\BillwerkEntitiesHelper $billwerkEntitiesHelper
   *   The BillwerkEntitiesHelper.
   * @param \Drupal\billwerk_subscriptions\Environment $environment
   *   The environment.
   *
   * @return \Drupal\billwerk_subscriptions_entities\SubscriberBillwerkEntitiesHelper
   *   The SubscriptionPlansManager object.
   */
  public static function create(Subscriber $subscriber, BillwerkEntitiesHelper $billwerkEntitiesHelper, Environment $environment): self {
    return new self(
    $subscriber,
    $billwerkEntitiesHelper,
    $environment,
     );
  }

  /**
   * Returns the subscriber's active BillwerkPlan.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlan
   *   The active BillwerkPlan or NULL if none.
   */
  public function getActiveBillwerkPlan(): ?BillwerkPlan {
    $activeBillwerkPlanVariant = NULL;
    if (!$this->subscriber->hasUserBillwerkContractId()) {
      return NULL;
    }
    $activeBillwerkPlanVariantId = $this->subscriber->getBillwerkContract()->getBillwerkContractSubscription()->getPlanVariantId();
    if (!empty($activeBillwerkPlanVariantId)) {
      $activeBillwerkPlanVariant = $this->billwerkEntitiesHelper->getBillwerkPlanVariantByPlanVariantId($activeBillwerkPlanVariantId, $this->environment, TRUE, FALSE);
      if (!empty($activeBillwerkPlanVariant)) {
        // Only Drupal-known plans:
        return $activeBillwerkPlanVariant->getBillwerkPlan();
      }
    }
    return NULL;
  }

  /**
   * Returns the subscriber's active BillwerkPlanVariant.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkPlanVariant
   *   The active BillwerkPlanVariant or NULL if none.
   */
  public function getActiveBillwerkPlanVariant(): ?BillwerkPlanVariant {
    $activeBillwerkPlanVariant = NULL;
    if (!$this->subscriber->hasUserBillwerkContractId()) {
      return NULL;
    }
    $activeBillwerkPlanVariantId = $this->subscriber->getBillwerkContract()->getBillwerkContractSubscription()->getPlanVariantId();
    if (!empty($activeBillwerkPlanVariantId)) {
      $activeBillwerkPlanVariant = $this->billwerkEntitiesHelper->getBillwerkPlanVariantByPlanVariantId($activeBillwerkPlanVariantId, $this->environment, TRUE, FALSE);
      if (!empty($activeBillwerkPlanVariant)) {
        // Only Drupal-known plan variants:
        return $activeBillwerkPlanVariant;
      }
    }
    return NULL;
  }

  /**
   * Returns the Subscriber's active BillwerkComponents.
   *
   * @param bool $onlyUnsubscribeable
   *   If TRUE, only return unsubscribeable components.
   *
   * @return \Drupal\billwerk_subscriptions_entities\Entity\BillwerkComponent[]
   *   The BillwerkComponents or an empty array, if none active.
   */
  public function getActiveBillwerkComponents(bool $onlyUnsubscribeable = FALSE): array {
    $activeBillwerkComponents = [];
    if (!$this->subscriber->hasUserBillwerkContractId()) {
      return [];
    }
    $activeBillwerkComponentSubscriptions = $this->subscriber->getBillwerkContract()->getBillwerkContractSubscription()->getComponentSubscriptions();
    if (!empty($activeBillwerkComponentSubscriptions)) {
      foreach ($activeBillwerkComponentSubscriptions as $subscriberCurrentComponentSubscription) {
        if ($onlyUnsubscribeable && !$subscriberCurrentComponentSubscription->isUnsubscribeable()) {
          continue;
        }
        $componentEntity = $this->billwerkEntitiesHelper->getBillwerkComponentByComponentId($subscriberCurrentComponentSubscription->getComponentId(), $this->environment, TRUE, FALSE);
        if (!empty($componentEntity)) {
          // Only add Drupal-known Billwerk Component entities:
          $activeBillwerkComponents[$componentEntity->getMachineName()] = $componentEntity;
        }
      }
    }
    return $activeBillwerkComponents;
  }

}
