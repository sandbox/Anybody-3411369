<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Exception;

/**
 * Subscriber Exception.
 */
class SubscriberException extends \Exception {

}
