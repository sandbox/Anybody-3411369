<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Exception;

/**
 * DataObject Exception.
 */
class DataObjectException extends \Exception {

}
