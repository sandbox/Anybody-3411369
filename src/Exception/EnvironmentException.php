<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Exception;

/**
 * Environment Exception.
 */
class EnvironmentException extends \Exception {

}
