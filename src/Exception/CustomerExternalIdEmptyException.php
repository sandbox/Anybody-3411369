<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Exception;

/**
 * Exception thrown when a Billwerk customer has an empty external ID.
 *
 * This exception is used to signal that the required external ID for a
 * Billwerk customer is missing during an operation.
 */
class CustomerExternalIdEmptyException extends \Exception {

}
