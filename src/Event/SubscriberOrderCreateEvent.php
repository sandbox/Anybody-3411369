<?php

namespace Drupal\billwerk_subscriptions\Event;

use Drupal\billwerk_subscriptions\Subscriber;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * High level event, fired on several occasions.
 *
 * It is fired before an order is created initially or as
 * up / downgrade for a contract or to add / remove a contract component.
 * See https://billwerk.readme.io/reference/orders_postorder_orderdto_post for
 * details.
 *
 * NOTE: This is only called for backend order operations, not for frontend
 * operations using SubscriptionJS!
 */
class SubscriberOrderCreateEvent extends Event {

  /**
   * An order which creates a fully new customer and contract.
   */
  const ORDER_TYPE_CREATE_CUSTOMER_CONTRACT = 'ORDER_TYPE_CREATE_CUSTOMER_CONTRACT';

  /**
   * An order which updates an existing subscription.
   *
   * For example changes the plan variant, subscribes or unsubscribes
   * components.
   */
  const ORDER_TYPE_UPDATE_SUBSCRIPTION = 'ORDER_TYPE_UPDATE_SUBSCRIPTION';

  /**
   * Constructor.
   *
   * @param string $orderType
   *   The order type (from the class constants)
   * @param array $orderData
   *   The prepared order data. An order array accepting the values from
   *   https://billwerk.readme.io/reference/orders_postorder_orderdto_post
   *   Allowed to be altered before being sent.
   * @param mixed $subscriber
   *   The subscriber to run the order for. Might not have a subscription /
   *   order yet, typically!
   * @param bool $previewOrder
   *   Determines if the order is only for preview. This can be useful to get
   *   pricing information or to dry-run the order. Preview orders can not be
   *   committed ($commitOrder)!
   * @param bool $commitOrder
   *   Determines if the order should be committed directly.
   *   This does NOT work combined with $previewOrder!
   */
  public function __construct(
    protected readonly string $orderType,
    // This may be altered by subscribers, therefor NOT readonly:
    protected array $orderData,
    protected readonly Subscriber $subscriber,
    protected bool $previewOrder,
    protected bool $commitOrder,
  ) {
  }

  /**
   * Returns the order data array.
   *
   * @return array
   *   The order data array.
   */
  public function getOrderData(): array {
    return $this->orderData;
  }

  /**
   * Sets the order data array.
   *
   * @param array $orderData
   *   The order data array.
   *
   * @return self
   *   The SubscriberOrderCreateEvent.
   */
  public function setOrderData(array $orderData): self {
    $this->orderData = $orderData;
    return $this;
  }

  /**
   * Returns the Subscriber.
   *
   * @return \Drupal\billwerk_subscriptions\Subscriber
   *   The subscriber.
   */
  public function getSubscriber(): Subscriber {
    return $this->subscriber;
  }

  /**
   * Returns the order type (constant).
   *
   * @return string
   *   The order type.
   */
  public function getOrderType(): string {
    return $this->orderType;
  }

  /**
   * Returns if the order should be committed.
   *
   * @return bool
   *   TRUE if the order should be committed, FALSE if not.
   */
  public function getCommitOrder(): bool {
    return $this->commitOrder;
  }

  /**
   * Sets if the order should be committed.
   *
   * @param bool $commit
   *   TRUE if the order should be committed, FALSE if not.
   *
   * @return self
   *   The SubscriberOrderCreateEvent.
   */
  public function setCommitOrder(bool $commit): self {
    $this->commitOrder = $commit;
    return $this;
  }

  /**
   * Returns if the order is only for preview.
   *
   * @return bool
   *   TRUE if the order is only for preview, FALSE if not.
   */
  public function getPreviewOrder(): bool {
    return $this->previewOrder;
  }

  /**
   * Sets if the order is only for preview.
   *
   * @param bool $preview
   *   TRUE if the order is only for preview, FALSE if not.
   *
   * @return self
   *   The SubscriberOrderCreateEvent.
   */
  public function setPreviewOrder(bool $preview): self {
    $this->previewOrder = $preview;
    return $this;
  }

}
