<?php

namespace Drupal\billwerk_subscriptions\Event;

use Drupal\billwerk_subscriptions\Subscriber;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * High level event, fired if a User's customer data changed at Billwerk.
 *
 * This event is fired when a change to a User's Billwerk Customer data was
 * detected, so that you can update the relevant fields in Drupal without
 * having to mess around with the lower level webhooks!
 *
 * Typically you may want to use this for things like:
 * -
 *
 * This will not be called, if a new Customer is created or a Customer is
 * entirely deleted.
 */
class SubscriberCustomerChangedEvent extends Event {

  /**
   * Constructor.
   *
   * @param \Drupal\billwerk_subscriptions\Subscriber $subscriber
   *   The subscriber.
   */
  public function __construct(
    protected readonly Subscriber $subscriber,
  ) {
  }

  /**
   * Gets the Subscriber whose Customer data changed.
   *
   * @return \Drupal\billwerk_subscriptions\Subscriber
   *   The subscriber.
   */
  public function getSubscriber(): Subscriber {
    return $this->subscriber;
  }

}
