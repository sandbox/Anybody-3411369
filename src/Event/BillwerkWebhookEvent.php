<?php

namespace Drupal\billwerk_subscriptions\Event;

use Drupal\Component\Serialization\Json;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired, if a webhook was called by Billwerk (incoming).
 *
 * This is a low level event.
 */
class BillwerkWebhookEvent extends Event {

  const EVENT_NAME_PREFIX = 'BillwerkWebhookEvent.';

  /**
   * The decoded data passed to the webhook.
   *
   * @var mixed
   */
  protected readonly mixed $data;

  /**
   * The Billwerk event name.
   *
   * @var string
   */
  protected readonly string $eventName;

  /**
   * The constructor.
   *
   * @param string $rawDataString
   *   The raw data string.
   */
  public function __construct(protected readonly string $rawDataString) {
    $this->data = Json::decode($rawDataString);
    $this->eventName = $this->extractDataValue('Event');
  }

  /**
   * Extracts a data value from the data array.
   *
   * @param string $key
   *   The key of the value to extract.
   *
   * @return mixed
   *   The extracted value.
   */
  public function extractDataValue(string $key): mixed {
    return $this->data[$key] ?? NULL;
  }

  /**
   * Returns the data.
   *
   * @return mixed
   *   The data.
   */
  public function getData(): mixed {
    return $this->data;
  }

  /**
   * Returns the raw data string.
   *
   * @return string
   *   The raw data string.
   */
  public function getRawDataString(): string {
    return $this->rawDataString;
  }

  /**
   * Returns the event name.
   *
   * @return string
   *   The event name.
   */
  public function getEventName(): string {
    return $this->eventName;
  }

}
