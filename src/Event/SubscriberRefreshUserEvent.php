<?php

namespace Drupal\billwerk_subscriptions\Event;

use Drupal\billwerk_subscriptions\Subscriber;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event, fired when a Drupal user's subscription should be checked for changes.
 *
 * This is a high level event.
 *
 * This event should be used to implement the user changes, like:
 * - Setting / removing roles
 * - Updating user data from the Billwerk values
 * - ...
 *
 * This event is called whenever it makes sense to refresh the user account
 * details with possible changes from Billwerk.
 *
 * It is dispatched from Subscriber::refreshFromBillwerkContractSubscription()
 * which is triggered in cases like:
 * - Billwerk Contract Changed
 * - Billwerk Customer Changed
 * - User Login
 * - Triggered in the user account by an administrator
 * - Triggered in custom implementation
 * - ...
 */
class SubscriberRefreshUserEvent extends Event {

  /**
   * The constructor.
   *
   * @param \Drupal\billwerk_subscriptions\Subscriber $subscriber
   *   The subscriber.
   */
  public function __construct(
    protected readonly Subscriber $subscriber,
  ) {
  }

  /**
   * Returns the subscriber.
   *
   * @return \Drupal\billwerk_subscriptions\Subscriber
   *   The subscriber.
   */
  public function getSubscriber(): Subscriber {
    return $this->subscriber;
  }

}
