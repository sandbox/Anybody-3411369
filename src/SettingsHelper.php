<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Site\Settings;

/**
 * Helper for settings retrieval from settings.php and config.
 */
final class SettingsHelper {

  /**
   * The settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs an SettingsHelper object.
   */
  public function __construct(
    protected readonly Settings $settings,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->config = $configFactory->get('billwerk_subscriptions.settings');
  }

  /**
   * Summary of getConfig.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config.
   */
  public function getConfig(): ImmutableConfig {
    return $this->config;
  }

  /**
   * Summary of getConfigValue.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The config value.
   */
  public function getConfigValue(string $key = ''): mixed {
    return $this->getConfig()->get($key);
  }

  /**
   * Summary of getEnvironment.
   *
   * @return string
   *   The environment config value.
   */
  public function getEnvironment(): string {
    return $this->getConfigValue('environment');
  }

  /**
   * Summary of getApiResponseCacheEnabled.
   *
   * @return bool
   *   Whether the API response cache is enabled.
   */
  public function getApiResponseCacheEnabled(): bool {
    return $this->getConfigValue('api_response_cache_enabled');
  }

  /**
   * Summary of getLoggingLevels.
   *
   * @return array
   *   The logging levels.
   */
  public function getLoggingLevels(): array {
    return $this->getConfigValue('logging_levels');
  }

  /**
   * Summary of getSettings.
   *
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *   The billwerk subscriptions settings.
   */
  public function getSettings(mixed $default = NULL): mixed {
    return Settings::get('billwerk_subscriptions.settings', $default);
  }

  /**
   * Summary of getSettingValue.
   *
   * @param string $name
   *   The settings name.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *   The setting value.
   */
  public function getSettingValue(string $name, mixed $default = NULL): mixed {
    $value = $default;
    $settings = Settings::get('billwerk_subscriptions.settings');
    if (is_array($settings) && isset($settings[$name])) {
      $value = $settings[$name];
    }
    return $value;
  }

  /**
   * Summary of getSandboxClientId.
   *
   * @throws \Exception
   *
   * @return string
   *   The sandbox client ID.
   */
  public function getSandboxClientId(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientId = $credentials['sandbox']['client_id'];
    if (empty($clientId)) {
      throw new \Exception('Sandbox client ID could not be determined, please check your settings.');
    }
    return $clientId;
  }

  /**
   * Summary of getSandboxClientSecret.
   *
   * @throws \Exception
   *
   * @return string
   *   The sandbox client secret.
   */
  public function getSandboxClientSecret(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientSecret = $credentials['sandbox']['client_secret'];
    if (empty($clientSecret)) {
      throw new \Exception('Sandbox client secret could not be determined, please check your settings.');
    }
    return $clientSecret;
  }

  /**
   * Summary of getProductionClientId.
   *
   * @throws \Exception
   *
   * @return string
   *   The production client ID.
   */
  public function getProductionClientId(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientId = $credentials['production']['client_id'];
    if (empty($clientId)) {
      throw new \Exception('Production client ID could not be determined, please check your settings.');
    }
    return $clientId;
  }

  /**
   * Summary of getProductionClientSecret.
   *
   * @throws \Exception
   *
   * @return string
   *   The production client secret.
   */
  public function getProductionClientSecret(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientSecret = $credentials['production']['client_secret'];
    if (empty($clientSecret)) {
      throw new \Exception('Production client secret could not be determined, please check your settings.');
    }
    return $clientSecret;
  }

  /**
   * Get the sandbox self service public api key.
   *
   * @throws \Exception
   *
   * @return string
   *   The sandbox self service public api key.
   */
  public function getSandboxSelfservicePublicApiKey(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientSecret = $credentials['sandbox']['selfservice_public_api_key'];
    if (empty($clientSecret)) {
      throw new \Exception('Production selfservice public API key could not be determined, please check your settings.');
    }
    return $clientSecret;
  }

  /**
   * Get the production self service public api key.
   *
   * @throws \Exception
   *
   * @return string
   *   The production self service public api key.
   */
  public function getProductionSelfservicePublicApiKey(): string {
    $credentials = $this->getSettingValue('credentials');
    $clientSecret = $credentials['production']['selfservice_public_api_key'];
    if (empty($clientSecret)) {
      throw new \Exception('Production selfservice public API key could not be determined, please check your settings.');
    }
    return $clientSecret;
  }

  /**
   * Get the webhook secret.
   *
   * @throws \Exception
   *
   * @return string
   *   The webhook secret.
   */
  public function getWebhookSecret(): string {
    $webhookSecret = $this->getSettingValue('webhook_secret');
    if (empty($webhookSecret)) {
      throw new \Exception('Webhook secret could not be determined.');
    }
    return rawurlencode(trim($webhookSecret));
  }

}
