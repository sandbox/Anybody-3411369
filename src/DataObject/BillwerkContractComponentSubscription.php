<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\DataObject;

/**
 * The Billwerk Contract Component Subscription Data Object (DTO).
 *
 * A Billwerk Contract Subscription can have zero to multiple of such
 * component subscriptions (aka Add-on's).
 *
 * We currently only support QuantityBased and OnOff components, no metered
 * components.
 *
 * Only contains the subset of fields relevant for the module's typical cases.
 * If you need additional values, consider requesting them from the API
 * or open an issue with good enough reasons. ;)
 *
 * Example (https://swagger.billwerk.io/#/operations/Contracts/Contracts_GetSubscriptions_id_timestamp_GET):
 * {
 *   "Id": "599d51f881b1f00a28f7ae8n",
 *   "ContractId": "599d51f881b1f00a28f7ae14",
 *   "CustomerId": "599d51f881b1f00a28f7asdf",
 *   "ComponentId": "599d51f881b1f00a28f7ae11",
 *   "Quantity": 0,
 *   "StartDate": "2023-11-28T06:01:25.3279049Z",
 *   "BilledUntil": "2024-01-28T06:01:25.3279046Z",
 *   "Status": "Active",
 *   "EndDate": "2024-06-28T06:01:25.3279047Z",
 *   "Memo": "Memoexample"
 * }
 *
 * Notes:
 * - EndDate is present, if the ending is scheduled (discontinued)
 * - If status is not active, the used call will not return the subscription at
 *   all, so we don't need to care for "Status"
 */
final class BillwerkContractComponentSubscription {

  /**
   * Constructor.
   *
   * @param string $id
   *   The id of the subscription.
   * @param string $componentId
   *   The id of the component.
   * @param int $quantity
   *   The quantity of the component.
   * @param ?string $startDate
   *   The start date of the subscription.
   * @param ?string $billedUntil
   *   The date until which the subscription is billed.
   * @param ?string $endDate
   *   The end date of the subscription.
   */
  public function __construct(
    protected readonly string $id,
    protected readonly string $componentId,
    protected readonly int $quantity,
    protected readonly ?string $startDate,
    protected readonly ?string $billedUntil,
    protected readonly ?string $endDate,
  ) {
  }

  /**
   * Checks if the subscription is unsubscribeable.
   *
   * @return bool
   *   TRUE if the subscription is unsubscribeable, FALSE otherwise.
   */
  public function isUnsubscribeable(): bool {
    // Can not be unsubscribed, if already unsubscribed.
    return empty($this->getEndDate());
  }

  /**
   * Returns this Billwerk Contract Component Subscription id.
   *
   * Important: This is NOT the component id, but the component subscription
   * id!
   *
   * @return string
   *   The id of the subscription.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Returns this Billwerk Contract Component Subscription Component id.
   *
   * @return string
   *   The id of the component.
   */
  public function getComponentId(): string {
    return $this->componentId;
  }

  /**
   * Returns the quantity.
   *
   * Returns the quantity of this Billwerk Contract Component Subscription
   * component id.
   *
   * @return int
   *   The quantity of the component.
   */
  public function getQuantity(): int {
    return $this->quantity;
  }

  /**
   * Returns the StartDate.
   *
   * @return string|null
   *   The start date of the subscription.
   */
  public function getStartDate(): ?string {
    return $this->startDate;
  }

  /**
   * Returns the BilledUntil.
   *
   * @return string|null
   *   The date until which the subscription is billed.
   */
  public function getBilledUntil(): ?string {
    return $this->billedUntil;
  }

  /**
   * Returns the EndDate.
   *
   * @return string|null
   *   The end date of the subscription.
   */
  public function getEndDate(): ?string {
    return $this->endDate;
  }

}
