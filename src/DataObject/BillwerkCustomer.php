<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\DataObject;

/**
 * The Billwerk Customer Data Object (DTO).
 *
 * Only contains the subset of fields relevant for the module's typical cases.
 * If you need additional values, consider requesting them from the API
 * or open an issue with good enough reasons. ;)
 *
 * Example
 * (https://swagger.billwerk.io/#/operations/Customers/Customers_Get_id_GET):
 * {
 *   "Id": "5996a94681b200088ca84f1a",
 *   "CreatedAt": "2023-12-28T06:01:25.5967706Z",
 *   "IsDeletable": false,
 *   "DeletedAt": "0001-01-01T00:00:00.0000000Z",
 *   "IsLocked": false,
 *   "CustomerName": "Wallace, Marcellus",
 *   "CustomerSubName": "",
 *   "FirstName": "Marcellus",
 *   "LastName": "Wallace",
 *   "EmailAddress": "marcellus@example.org",
 *   "AdditionalEmailAddresses": [
 *     {
 *       "EmailAddress": "marcellus02@example.com"
 *     },
 *     {
 *       "EmailAddress": "marcellus03@example.com"
 *     }
 *   ],
 *   "Address": {
 *     "Street": "Raymond Ave (Holly)",
 *     "HouseNumber": "145",
 *     "PostalCode": "91001",
 *     "City": "Pasadena",
 *     "Country": "CA"
 *   },
 *   "AdditionalAddresses": [
 *     {
 *       "FirstName": "Martin",
 *       "LastName": "Schmidt",
 *       "CompanyName": "Billwerk",
 *       "Street": "Schillergasse",
 *       "HouseNumber": "17",
 *       "PostalCode": "89278",
 *       "City": "Frankfurt am Main",
 *       "Country": "DE"
 *     }
 *   ],
 *   "Locale": "en",
 *   "CustomFields": {
 *     "CustomFieldName": "CustomFieldValue",
 *     "Name": "Value"
 *   },
 *   "DefaultBearerMedium": "Email",
 *   "CustomerType": "Consumer",
 *   "Hidden": false
 * }
 */
final class BillwerkCustomer {

  /**
   * Constructor.
   *
   * @param string $id
   *   The Billwerk Customer (primary) ID.
   * @param string $externalCustomerId
   *   The Billwerk Customer ExternalCustomerId (= Drupal UID reference).
   * @param string $emailAddress
   *   The Billwerk Customer Email Address.
   * @param string|null $locale
   *   The Billwerk Customer locale code, e.g. "en".
   * @param bool $isDeletable
   *   Tells if the Billwerk Customer is deletable.
   * @param bool $isLocked
   *   Tells if the Billwerk Customer is locked.
   * @param bool $hidden
   *   Tells if the Billwerk Customer is hidden.
   * @param string|null $deletedAt
   *   The date when the contract was deleted (if it was). Else NULL.
   */
  public function __construct(
    protected readonly string $id,
    protected readonly string $externalCustomerId,
    protected readonly string $emailAddress,
    protected readonly ?string $locale,
    protected readonly bool $isDeletable,
    protected readonly bool $isLocked,
    protected readonly bool $hidden,
    protected readonly ?string $deletedAt,
  ) {
  }

  /**
   * Get the Billwerk Customer (primary) ID.
   *
   * @return string
   *   The Billwerk Customer (primary) ID.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Get the Billwerk Customer ExternalCustomerId (= Drupal UID reference).
   *
   * Please note that this is returning a string that needs to be casted to
   * int typically if used as Drupal User ID.
   *
   * @return string
   *   The Billwerk Customer ExternalCustomerId.
   */
  public function getExternalCustomerId(): string {
    return $this->externalCustomerId;
  }

  /**
   * Get the Billwerk Customer Email Address.
   *
   * @return string
   *   The Billwerk Customer Email Address.
   */
  public function getEmailAddress(): string {
    return $this->emailAddress;
  }

  /**
   * Get the Billwerk Customer locale code, e.g. "en".
   *
   * @return string|null
   *   The Billwerk Customer locale code.
   */
  public function getLocale(): ?string {
    return $this->locale;
  }

  /**
   * Tells if the Billwerk Customer is deletable.
   *
   * @return bool
   *   Whether the Billwerk Customer is deletable.
   */
  public function getIsDeletable(): bool {
    return $this->isDeletable;
  }

  /**
   * Tells if the Billwerk Customer is locked.
   *
   * @return bool
   *   Whether the Billwerk Customer is locked.
   */
  public function getIsLocked(): bool {
    return $this->isLocked;
  }

  /**
   * Tells if the Billwerk Customer is hidden.
   *
   * @return bool
   *   Whether the Billwerk Customer is hidden.
   */
  public function getIsHidden(): bool {
    return $this->hidden;
  }

  /**
   * Returns the date when the contract was deleted (if it was). Else NULL.
   *
   * @return string|null
   *   The date when the contract was deleted (if it was). Else NULL.
   */
  public function getDeletedAt(): ?string {
    return $this->deletedAt ?: NULL;
  }

  /**
   * Returns true if the contract was deleted, else false.
   *
   * @return bool
   *   Whether the contract was deleted.
   */
  public function isDeleted(): bool {
    return !empty($this->getDeletedAt());
  }

}
