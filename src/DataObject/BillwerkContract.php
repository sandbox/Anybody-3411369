<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\DataObject;

/**
 * The Billwerk Contract Data Object (DTO).
 *
 * This represents the Billwerk Contract, which is the central element in our
 * implementation, as each Drupal user should only have one (representative)
 * Contract for their subscription. For that reason, all other information
 * is linked from this Billwerk Contract as central element:
 * - Billwerk Customer
 * - Billwerk ContractSubscriptions (subscriptions part of the contract), with
 * further subelements.
 *
 * Only contains the subset of fields relevant for the module's typical cases.
 * If you need additional values, consider requesting them from the API
 * or open an issue with good enough reasons. ;)
 *
 * Example
 * (https://swagger.billwerk.io/#/operations/Contracts/Contracts_Get_id_GET):
 * {
 *   "Id": "599d51f881b1f00a28f7ae9e",
 *   "LastBillingDate": "2023-12-28T06:01:25.2854851Z",
 *   "NextBillingDate": "2023-12-28T06:01:25.2854856Z",
 *   "PlanId": "599d51f881b1f00a28f7ae9f",
 *   "CustomerId": "599d51f881b1f00a28f7ae9g",
 *   "IsDeletable": false,
 *   "LifecycleStatus": "Active",
 *   "CustomerName": "Marcellus Wallace",
 *   "CustomerIsLocked": false,
 *   "Phases": [
 *     {
 *       "Type": "Normal",
 *       "StartDate": "2023-12-28T06:01:25.2854877Z",
 *       "PlanVariantId": "599d51f881b1f00a28f7ae9h",
 *       "PlanId": "599d51f881b1f00a28f7ae9i",
 *       "InheritStartDate": false
 *     },
 *     {
 *       "Type": "Trial",
 *       "StartDate": "2023-12-28T06:01:25.2854886Z",
 *       "PlanVariantId": "599d51f881b1f00a28f7ae9j",
 *       "PlanId": "599d51f881b1f00a28f7ae9k",
 *       "InheritStartDate": false
 *     }
 *   ],
 *   "Balance": 0,
 *   "Currency": "EUR",
 *   "PlanGroupId": "599d51f881b1f00a28f7ae9l",
 *   "PaymentBearer": {
 *     "CardType": "Visa",
 *     "ExpiryMonth": 12,
 *     "ExpiryYear": 2020,
 *     "Holder": "Marcellus Wallace",
 *     "Last4": "1234",
 *     "Type": "CreditCard",
 *     "Country": "DE"
 *   },
 *   "PaymentProvider": "PayOne",
 *   "EscalationSuspended": false,
 *   "RecurringPaymentsPaused": false,
 *   "CurrentPhase": {
 *     "Type": "Normal",
 *     "StartDate": "2023-12-28T06:01:25.2854910Z",
 *     "PlanVariantId": "599d51f881b1f00a28f7ae9m",
 *     "PlanId": "599d51f881b1f00a28f7ae9n",
 *     "InheritStartDate": false
 *   },
 *   "PaymentProviderSupportRefunds": false,
 *   "BillingSuspended": false,
 *   "ThresholdBillingDisabled": false,
 *   "TimeGranularity": "Precise",
 *   "StartDate": "2023-12-28T06:01:25.2854916Z",
 *   "EndDate": "2023-12-28T06:01:25.2854919Z",
 *   "BilledUntil": "2023-12-28T06:01:25.2854920Z",
 *   "PlanVariantId": "599d51f881b1f00a28f7ae9o",
 *   "Notes": "NoteExample"
 * }
 */
final class BillwerkContract {

  const LIFECYCLE_STATUS_ACTIVE = 'Active';
  const LIFECYCLE_STATUS_INTRIAL = 'InTrial';
  const LIFECYCLE_STATUS_PAUSED = 'Paused';
  const LIFECYCLE_STATUS_TRIAL_EXPIRED = 'TrialExpired';
  const LIFECYCLE_STATUS_ANNULLED = 'Annulled';

  /**
   * Billwerk Contract constructor.
   *
   * @param string $id
   *   The Billwerk Contract ID.
   * @param string $lifecycleStatus
   *   The Billwerk Contract current lifecycle status.
   * @param \Drupal\billwerk_subscriptions\DataObject\BillwerkCustomer $billwerkCustomer
   *   The Billwerk Contract owner BillwerkCustomer data object.
   * @param \Drupal\billwerk_subscriptions\DataObject\BillwerkContractSubscription $billwerkContractSubscription
   *   The BillwerkContractSubscription of this Contract.
   */
  public function __construct(
    protected readonly string $id,
    protected readonly string $lifecycleStatus,
    protected readonly BillwerkCustomer $billwerkCustomer,
    protected readonly BillwerkContractSubscription $billwerkContractSubscription,
  ) {
  }

  /**
   * Get the Billwerk Contract ID.
   *
   * @return string
   *   The Billwerk Contract ID.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Get the Billwerk Contract current lifecycle status.
   *
   * Also note the BillwerkContractSubscription::$phaseType which might be
   * clearer.
   *
   * @return string
   *   The Billwerk Contract current lifecycle status.
   */
  public function getLifecycleStatus(): string {
    return $this->lifecycleStatus;
  }

  /**
   * Get the Billwerk Contract owner BillwerkCustomer data object.
   *
   * @return \Drupal\billwerk_subscriptions\DataObject\BillwerkCustomer
   *   The Billwerk Contract owner BillwerkCustomer data object.
   */
  public function getBillwerkCustomer(): BillwerkCustomer {
    return $this->billwerkCustomer;
  }

  /**
   * Returns the BillwerkContractSubscription of this Contract.
   *
   * @return \Drupal\billwerk_subscriptions\DataObject\BillwerkContractSubscription
   *   The BillwerkContractSubscription of this Contract.
   */
  public function getBillwerkContractSubscription(): BillwerkContractSubscription {
    return $this->billwerkContractSubscription;
  }

  /**
   * Returns true if the Billwerk Contract is currently active.
   *
   * This can for example mean, the contract is active or in trial.
   *
   * @return bool
   *   True if the Billwerk Contract is currently active.
   */
  public function isActive(): bool {
    return in_array($this->getLifecycleStatus(), [self::LIFECYCLE_STATUS_ACTIVE, self::LIFECYCLE_STATUS_INTRIAL]);
  }

  /**
   * Returns true if the Billwerk Contract is currently in trial.
   *
   * @return bool
   *   True if the Billwerk Contract is currently in trial.
   */
  public function isInTrial(): bool {
    return $this->getLifecycleStatus() === self::LIFECYCLE_STATUS_INTRIAL;
  }

}
