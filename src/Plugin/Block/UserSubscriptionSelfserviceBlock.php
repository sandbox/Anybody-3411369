<?php

declare(strict_types=1);

namespace Drupal\billwerk_subscriptions\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\billwerk_subscriptions\EmbedHelper;
use Drupal\billwerk_subscriptions\Subscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a billwerk subscriptions: manage subscription (selfservice) block.
 *
 * This is only for the current user.
 *
 * @Block(
 *   id = "billwerk_subscriptions_user_subscription_selfservice_block",
 *   admin_label = @Translation("Billwerk Subscriptions: Manage Subscription (Selfservice)"),
 *   category = @Translation("Billwerk Subscriptions"),
 * )
 */
final class UserSubscriptionSelfserviceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly AccountInterface $currentUser,
    private readonly EmbedHelper $embedHelper,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('billwerk_subscriptions.embed_helper'),
    );
  }

  /**
   * Shows the self service portal for a single subscription of the given user.
   *
   * @return array
   *   The Drupal render array.
   */
  public function build(): array {
    $subscriber = Subscriber::loadByDrupalUid((int) $this->currentUser->id());
    $build = $this->embedHelper->buildSelfserviceManageContractEmbed($subscriber);
    return $build;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function blockAccess(AccountInterface $account) {
    // Warning: $account is the current user accessing this!
    $access = AccessResult::forbidden();
    if ($account->id() === $this->currentUser->id()) {
      // Is the current users account.
      $access = AccessResult::allowedIfHasPermission($account, 'billwerk_subscriptions_selfservice_manage_own_contract');
    }
    else {
      // Is a different users account.
      $access = AccessResult::allowedIfHasPermission($account, 'billwerk_subscriptions_selfservice_manage_any_contract');
    }
    return $access->andIf(AccessResult::allowedIf(Subscriber::loadByDrupalUid((int) $this->currentUser->id())->hasUserBillwerkContractId()));
  }

}
